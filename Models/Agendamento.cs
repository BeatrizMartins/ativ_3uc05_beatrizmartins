using System;
using System.Collections.Generic;
using System.Linq;

namespace ATIV_2_BEATRIZMARTINS.Models  
{
    public class Agendamento 
    {
        public string Nome {get ; set; }
        public int Telefone {get ; set; }
        public DateTime Data{get ; set; }
        public string Animal {get ; set; }
        public string Necessidade {get ; set; }

    }
}